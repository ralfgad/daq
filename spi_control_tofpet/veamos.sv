module otra_prueba
  (input entrada,
  output logic salida
  )

logic idea;
  always_ff@(posedge clk)
  idea<= entrada;

  assign salida=idea;
endmodule
